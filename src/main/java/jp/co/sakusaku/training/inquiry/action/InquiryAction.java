package jp.co.sakusaku.training.inquiry.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.sakusaku.training.inquiry.actionform.InquiryForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class InquiryAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse resp) throws Exception {

		InquiryForm inquiryForm = (InquiryForm) form;
		req.setAttribute("form", inquiryForm);

		return mapping.findForward("success");
	}
}