package jp.co.sakusaku.training.inquiry.actionform;

import org.apache.struts.validator.ValidatorForm;


public class InquiryForm extends ValidatorForm {

    private String title;
    private String content;
    private String name;
    private String phone;


    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
    	this.name = name;
    }
    public String getPhone() {
    	return phone;
    }
    public void setPhone(String phone){
    	this.phone = phone;
    }

}